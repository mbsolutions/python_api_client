from __future__ import unicode_literals

from .classes import API  # NOQA
from .exceptions import *  # NOQA

__author__ = 'Roberto Rosario'
__email__ = 'roberto.rosario@mayan-edms.com'
__version__ = '0.5.0'
